INTRODUCTION
------------

Commerce Satispay

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_satispay

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_satispay


REQUIREMENTS
------------

This module requires the following modules:

 * Drupal Commerce (https://www.drupal.org/project/commerce)
 * Libraries API (https://www.drupal.org/project/libraries)

This module requires the following external library:

 * Satispay Online API PHP SDK (https://github.com/satispay/online-api-php-sdk)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download the Satispay Online API PHP SDK from Github
   (https://github.com/satispay/online-api-php-sdk) and extract, rename the
   extracted folder to "online-api-php-sdk" and copy it into
   "sites/all/libraries/satispay". The API should now be located at
   "sites/all/libraries/satispay/online-api-php-sdk/init.php".


NOTES
-----

The Satispay Online API PHP SDK have been deprecated and replace the new API
and a new library with a different workflow.
Even if deprecated the system is kept active, given the EOF of Drupal 7,
for now, a refactoring of the module will not be done.


CONFIGURATION
-------------

Enable and configure via Payment methods form
(admin/commerce/config/payment-methods), you need to enter the Satispay Security
Bearer.

To create one for Production environment, you must login to the Dashboard,
create an Online Shop and then click on Create a Security Bearer.

Select for use the staging or production environment.
To request the creation of a SandBox account write to business@satispay.com.
Please make sure to indicate the mobile number and the email to be associated
with the SandBox account.

More documentation about Satispay Online API can be checked at:
https://s3-eu-west-1.amazonaws.com/docs.online.satispay.com/index.html


MAINTAINERS
-----------

Current maintainers:
 * Riccardo Bessone (bessone) - https://www.drupal.org/user/23887
