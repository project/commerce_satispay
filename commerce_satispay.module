<?php

/**
 * @file
 * Implements Satispay payment service for use with Drupal Commerce checkout.
 */

/**
 * Implements hook_menu().
 */
function commerce_satispay_menu() {
  $items = array();

  // Define an always accessible path to receive callback.
  $items['commerce_satispay/callback'] = array(
    'page callback' => 'commerce_satispay_callback',
    'page arguments' => array(),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  // Define an additional IPN path that is payment method / instance specific.
  $items['commerce_satispay/callback/%commerce_payment_method_instance'] = array(
    'page callback' => 'commerce_satispay_callback',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_satispay_commerce_payment_method_info() {
  $payment_methods = array();

  $icons = commerce_satispay_icons();
  $display_title = t('!logo Satispay', array('!logo' => $icons['satispay']));

  $payment_methods['commerce_satispay'] = array(
    'method_id' => 'commerce_satispay',
    'title' => t('Satispay'),
    'short_title' => t('Satispay'),
    'display_title' => $display_title,
    'description' => t('Satispay'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_satispay_settings_form($settings = NULL) {
  $form = array();

  $default_currency = variable_get('commerce_default_currency', 'EUR');
  $settings = (array) $settings + array(
    'commerce_satispay_security_bearer' => '',
    'commerce_satispay_staging' => 1,
    'commerce_satispay_currency' => in_array($default_currency, array_keys(commerce_satispay_currencies())) ? $default_currency : 'EUR',
    'commerce_satispay_method_title' => t('Satispay:'),
    'commerce_satispay_method_title_icons' => TRUE,
    'commerce_satispay_checkout_button' => t('Submit Order'),
  );

  $form['commerce_satispay_security_bearer'] = array(
    '#type' => 'textfield',
    '#title' => t('Security Bearer'),
    '#description' => t('Satispay security bearer.'),
    '#default_value' => $settings['commerce_satispay_security_bearer'],
    '#size' => 150,
    '#maxlength' => 255,
  );
  $form['commerce_satispay_staging'] = array(
    '#type' => 'select',
    '#title' => t('Staging'),
    '#description' => t('Use staging for testing, requires a Satispay Sandbox account'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => $settings['commerce_satispay_staging'],
  );
  $form['commerce_satispay_currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency preference'),
    '#description' => t('Adjust currency used with Satispay.'),
    '#options' => commerce_satispay_currencies(),
    '#default_value' => $settings['commerce_satispay_currency'],
  );
  $form['commerce_satispay_method_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment method title'),
    '#default_value' => $settings['commerce_satispay_method_title'],
  );
  $form['commerce_satispay_checkout_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Order review submit button text'),
    '#description' => t('Provide Satispay specific text for the submit button on the order review page.'),
    '#default_value' => $settings['commerce_satispay_checkout_button'],
  );

  return $form;
}

/**
 * Payment method callback: adds a message to the submission form.
 */
function commerce_satispay_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form['satispay_information'] = array(
    '#markup' => '<span class="commerce-satispay-info">' . t('(Continue with checkout to complete payment with Satispay.)') . '</span>',
  );

  return $form;
}

/**
 * Payment method callback.
 *
 * Redirect form, a wrapper around the module's general use
 * function for building a form.
 */
function commerce_satispay_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  if (empty($payment_method['settings']['commerce_satispay_security_bearer'])) {
    drupal_set_message(t('Satispay is not configured for use. No Satispay security bearer has been specified'), 'error');
    return array();
  }

  $settings = array(
    // Return to the previous page when payment is canceled.
    'cancel_return' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),

    // Return to the payment redirect page for processing successful payments.
    'return' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),

    // Specify the current payment method instance ID in the notify_url.
    'payment_method' => $payment_method['instance_id'],
  );

  return commerce_satispay_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

/**
 * Builds a Website Payments Standard form from an order object.
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal form state.
 * @param object $order
 *   The fully loaded order being paid for.
 * @param array $settings
 *   An array of settings used to build out the form, including:
 *   - commerce_satispay_alias: the Satispay store identification code
 *     to use
 *   - commerce_satispay_mac: the MAC that you got from Satispay after
 *     activation
 *   - commerce_satispay_mode: the option for a real or test transaction
 *   - commerce_satispay_currency: the Satispay currency ISO code to use
 *     for this payment
 *   - commerce_satispay_language: the Satispay language code to use
 *     on the payment form
 *   - return: the URL Satispay should send the user to on successful
 *     payment
 *   - cancel_return: the URL Satispay should send the user to on
 *     cancellation
 *   - payment_method: optionally the name of the Commerce payment method to
 *     include in the callback notify_url.
 *
 * @return array
 *   A renderable form array.
 */
function commerce_satispay_order_form(array $form, array &$form_state, $order, array $settings) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $settings['commerce_satispay_currency'];
  $order_currency_code = $wrapper->commerce_order_total->currency_code->value();

  $amount = $wrapper->commerce_order_total->amount->value();

  $library = libraries_load('satispay');

  \SatispayOnline\Api::setSecurityBearer($settings['commerce_satispay_security_bearer']);
  \SatispayOnline\Api::setSandbox($settings['commerce_satispay_staging']);

  $checkout = \SatispayOnline\Checkout::create(array(
    'description' => t('Order #@order_number', array('@order_number' => $wrapper->order_number->value())),
    'phone_number' => '',
    'redirect_url' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    'callback_url' => commerce_satispay_callback_url($settings['payment_method']),
    'amount_unit' => commerce_currency_convert($amount, $order_currency_code, $currency_code),
    'currency' => $currency_code,
    'metadata' => array(
      'order' => $wrapper->order_id->value(),
    ),
  ));

  if (!isset($checkout->id)) {
    // Failed.
    drupal_set_message(t('An error as occurred with the payment system.'), 'error');
    // Debug.
    watchdog(
      'commerce_satispay',
      'Checkout error for #@order_id: @checkout',
      array('@order_id' => $order->order_id, '@checkout' => json_encode($checkout)),
      WATCHDOG_NOTICE
    );
  }
  else {
    $form['#action'] = $checkout->checkout_url;
    $form['#method'] = 'get';
    $form['checkout_id'] = array(
      '#type' => 'hidden',
      '#value' => $checkout->id,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $settings['commerce_satispay_checkout_button'],
    );
    // Debug.
    watchdog(
      'commerce_satispay',
      'Generated checkout for order #@order_id: @checkout',
      array('@order_id' => $order->order_id, '@checkout' => json_encode($checkout)),
      WATCHDOG_NOTICE
    );
  }

  return $form;
}

/**
 * Processes an incoming transaction.
 *
 * @param string $payment_method
 *   The payment method instance array that originally made the payment.
 *
 * @return bool
 *   TRUE or FALSE indicating whether the callback was successfully processed.
 */
function commerce_satispay_callback($payment_method = NULL) {

  $query = drupal_get_query_parameters();
  $uuid = @$query['uuid'];

  // Check that the uuid variable is set.
  if (!isset($uuid)) {
    watchdog('commerce_satispay', 'Callback URL accessed with no UUID submitted.', array(), WATCHDOG_WARNING);
    return FALSE;
  }
  // If no payment method is passed, we can't verify.
  if ($payment_method === NULL) {
    // commerce_payment_redirect_pane_previous_page($order);
    watchdog('commerce_satispay', 'No payment method is passed, we can\'t verify.', array(), WATCHDOG_WARNING);
    return FALSE;
  }

  // Debug.
  watchdog('commerce_satispay', 'Callback for charge @uuid', array('@uuid' => $uuid), WATCHDOG_NOTICE);

  $settings = $payment_method["settings"];

  $library = libraries_load('satispay');

  \SatispayOnline\Api::setSecurityBearer($settings['commerce_satispay_security_bearer']);
  \SatispayOnline\Api::setSandbox($settings['commerce_satispay_staging']);

  $charge = \SatispayOnline\Charge::get($uuid);

  // Debug.
  watchdog('commerce_satispay', 'Loaded charge: @charge', array('@charge' => json_encode($charge)), WATCHDOG_NOTICE);

  // Load the order from commerce.
  $order = commerce_order_load($charge->metadata->order);

  // Error: Order NOT in checkout state.
  if ($order === FALSE) {
    watchdog('commerce_satispay',
             'Return from payment gateway - Order not in checkout state or not found, received response for payment from gateway but order #@order_id is not in "in_checkout" state',
             array('@order_id' => $order->order_id),
             WATCHDOG_ERROR
            );
    commerce_payment_redirect_pane_previous_page($order);
    return FALSE;
  }

  // Create a new payment transaction for the order.
  $transaction = commerce_payment_transaction_new('commerce_satispay', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->remote_id = $charge->id;
  $transaction->amount = $charge->amount;
  $transaction->currency_code = $charge->currency;
  $transaction->payload[REQUEST_TIME] = $charge;

  // Set the transaction's statuses based on the IPN's payment_status.
  $transaction->remote_status = $charge->status;

  $paid = FALSE;

  switch ($charge->status) {
    case 'REQUIRED':
      // Charge sent to a user waiting for acceptance.
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('Charge sent to a user waiting for acceptance.');
      break;

    case 'SUCCESS':
      // Charge accepted by the user.
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Charge accepted by the user.');
      $paid = TRUE;
      break;

    case 'FAILURE':
      // Charge failed, more details can be found on STATUS_DETAIL.
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      switch ($charge->status_detail) {
        case 'DECLINED_BY_PAYER':
          $status_detail = t('user declined the Charge.');
          break;

        case 'DECLINED_BY_PAYER_NOT_REQUIRED':
          $status_detail = t('user declined the Charge because he did not request it.');
          break;

        case 'CANCEL_BY_NEW_CHARGE':
          $status_detail = t('same Charge sent to the same user, the second will override the first.');
          break;

        case 'INTERNAL_FAILURE':
          $status_detail = t('generic error.');
          break;

        case 'EXPIRED':
          $status_detail = t('the Charge has expired.');
          break;

      }
      $transaction->message = t('The payment has failed. Detail: @status_detail',
                                array('@status_detail' => $status_detail)
                              );
      $transaction->remote_status .= ' - ' . $charge->status_detail;
      break;
  }

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);

  /* If payment was ok,redirect the user to next page,
   *  else redirect to previous page. */
  if ($paid) {
    commerce_payment_redirect_pane_next_page($order);
  }
  else {
    commerce_payment_redirect_pane_previous_page($order);
  }
  watchdog(
    'commerce_satispay',
    'Payment processed for Order @order_number with ID @charge_id.',
    array('@charge_id' => $charge->id, '@order_number' => $order->order_number),
    WATCHDOG_INFO
  );
}

/**
 * Returns the callback URL.
 *
 * @param string $method_id
 *   Optionally specify a payment method ID to include in the URL.
 */
function commerce_satispay_callback_url($method_id = NULL) {
  $parts = array(
    'commerce_satispay',
    'callback',
  );

  if (!empty($method_id)) {
    $parts[] = $method_id;
  }

  $callback_url = url(implode('/', $parts), array('query' => array('uuid' => '{uuid}'), 'absolute' => TRUE));
  // Workaround for braces encoding.
  $callback_url = str_replace('%7B', '{', $callback_url);
  $callback_url = str_replace('%7D', '}', $callback_url);

  return $callback_url;
}

/**
 * Returns an array of Satispay payment method icon img element.
 *
 * @return array
 *   A renderable img element array.
 */
function commerce_satispay_icons() {
  $icons = array();

  $variables = array(
    'path' => drupal_get_path('module', 'commerce_satispay') . '/images/pay_logo.png',
    'title' => t('Satispay'),
    'alt' => t('Satispay'),
    'height' => 20,
    'width' => 20,
    'attributes' => array(
      'class' => array('commerce-satispay-icon'),
    ),
  );
  $icons['satispay'] = theme('image', $variables);

  return $icons;
}

/**
 * Returns an array of all possible currency codes.
 */
function commerce_satispay_currencies() {
  return drupal_map_assoc(array('EUR'));
}

/**
 * Implements hook_libraries_info().
 */
function commerce_satispay_libraries_info() {
  return array(
    'satispay' => array(
      'name' => 'Satispay Online API PHP SDK',
      'vendor url' => 'https://www.satispay.com',
      'download url' => 'https://github.com/satispay/online-api-php-sdk',
      'path' => 'online-api-php-sdk',
      'version arguments' => array(
        'file' => 'online-api-php-sdk/lib/Api.php',
        'pattern' => "/version\s+=\s+(?:\"|')([0-9a-zA-Z\.-]+)(?:\"|')/",
        'lines' => 40,
      ),
      'files' => array(
        'php' => array('init.php'),
      ),
    ),
  );
}
